/**
 * @author Berend Hulshof, Alexandra Putuntica
 */
package nl.saxion.playground.ittycoon;

import nl.saxion.playground.ittycoon.adapters.ClientContractListAdapter;
import nl.saxion.playground.ittycoon.lib.Game;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ProgressBar;
import android.support.v7.widget.RecyclerView;

public class ClientActivity extends AppCompatActivity {

    private Game game = Game.getInstance();
    private RecyclerView contracts;

    /**
     * Oncreate method, initialises the view.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        TextView clientName = findViewById(R.id.clientNameAC);
        ProgressBar progressBar = findViewById(R.id.satisfactoryProgressBar);
        contracts = findViewById(R.id.clientContracts);

        String client = getIntent().getStringExtra("client");

        clientName.setText(game.getClient(client).getName());

        progressBar.setMax(100);
        progressBar.setProgress(game.getClient(client).getSatisfactory());

        contracts.setLayoutManager(new LinearLayoutManager(this));
        contracts.setAdapter(new ClientContractListAdapter(game.getClient(client).getContracts()));

        ImageView helpIcon = findViewById(R.id.helpIcon);

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(ClientActivity.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(ClientActivity.this);
                showImage.setImageResource(R.drawable.client_page);
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }

    /**
     * On resume function, this will refresh the contract recyclerview.
     */
    @Override
    public void onResume() {
        super.onResume();
        contracts.getAdapter().notifyDataSetChanged();
    }

    /**
     * Refresh the contract recyclerview.
     */
    public void doRefresh(){
        contracts.getAdapter().notifyDataSetChanged();
    }
}
