/**
 * @author Sergiu Waxmann, Alexandra Putuntica
 */
package nl.saxion.playground.ittycoon;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class EndGameActivity extends AppCompatActivity {

    /**
     * On create method, initialises game data.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(EndGameActivity.this,
                        WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 5000);

    }
}
