/**
 * @author Sergiu Waxmann
 */
package nl.saxion.playground.ittycoon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;

public class SpalshScreenActivity extends AppCompatActivity {

    /**
     * On create method, initialises the activity.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_spalsh);

        // Many thanks to Sergiu Waxmann for finding this method, this really helped me out - Berend Hulshof
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SpalshScreenActivity.this,
                        WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000); //View the splash screen for 3 seconds

    }
}
