/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.models;

import java.util.ArrayList;
import java.util.List;

import nl.saxion.playground.ittycoon.lib.Game;

public class Client {
    private String name;
    private List<Contract> contracts;
    private int satisfactory;

    public Client(String name) {
        this.name = name;
        this.contracts = new ArrayList<>();
        this.satisfactory = 50;
    }

    /**
     * Gets the client name
     * @return client name
     */
    public String getName() { return name; }

    /**
     * Adds new contract to the client.
     * @param contract to add
     */
    public void addNewContract(Contract contract) {
        this.contracts.add(contract);
    }

    /**
     * Remove contract from client.
     * @param contract to remove
     */
    public void removeContract(Contract contract) {
        this.contracts.remove(contract);
    }

    /**
     * Gets the list of client contracts.
     * @return list of client
     */
    public List<Contract> getContracts() {
        return contracts;
    }

    /**
     * Satisfy a client.
     * @param satisfactory amount to satisfy
     */
    public void satisfy(int satisfactory) {
        if(this.satisfactory != 100) {
            int newSatisfactory = this.satisfactory + satisfactory;
            if(newSatisfactory > 100 ) {
                this.satisfactory = 100;
            } else {
                this.satisfactory += satisfactory;
            }
        }
    }

    /**
     * Dissatisfy a client.
     * @param dissatisfactory amount to dissatisfy
     */
    public void dissatify(int dissatisfactory) {
        if(this.satisfactory != 0) {
            int newSatisfactory = this.satisfactory + satisfactory;
            if(newSatisfactory < 0) {
                this.satisfactory = 0;
            } else {
                this.satisfactory -= dissatisfactory;
            }
        } else {
            Game.getInstance().company.removeClient(this);
        }
    }

    /**
     * Get the current satisfactory level of the client.
     * @return client's satisfactory
     */
    public int getSatisfactory() {
        return satisfactory;
    }
}
