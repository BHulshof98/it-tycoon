/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.models;

public class Office {
    private int rent;
    private int workSpots;
    private int level;

    public Office(int rent, int workSpots, int level) {
        this.rent = rent;
        this.workSpots = workSpots;
        this.level = level;
    }

    /**
     * Gets the rent for the office.
     * @return rent for the office
     */
    public int getRent() {
        return rent;
    }

    /**
     * Checks if the company can hire more employees.
     * @param currentEmployees current amount of employees
     * @return boolean
     */
    public boolean canHire(int currentEmployees) {
        return currentEmployees < getWorkSpots();
    }

    /**
     * Gets the level of the office.
     * @return level of office
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets the amount of work spots in the office.
     * @return amount of work spots
     */
    public int getWorkSpots() {
        return workSpots;
    }
}
