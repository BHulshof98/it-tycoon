/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import nl.saxion.playground.ittycoon.EmployeeActivity;
import nl.saxion.playground.ittycoon.EmployeeItem;
import nl.saxion.playground.ittycoon.EmployeesActivity;
import nl.saxion.playground.ittycoon.R;
import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Employee;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.ViewHolder> {
    private List<Employee> employees;
    private boolean internal;

    public EmployeeListAdapter(List<Employee> employees, boolean internal) {
        this.employees = employees;
        this.internal = internal;
    }

    /**
     *  Create new views (invoked by the layout manager)
     * @param parent parent viewgroup (automatic)
     * @param viewType view type (automatic)
     * @return viewholder
     */
    @Override
    public EmployeeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // no need for a LayoutInflater instance—
        // the custom view inflates itself
        EmployeeItem itemView = new EmployeeItem(parent.getContext());
        return new ViewHolder(itemView, internal);
    }

    /**
     * Replace the contents of a view (invoked by the layout manager).
     * @param holder the viewholder
     * @param position position of item
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Employee employee = employees.get(position);
        holder.name.setText(employee.getName());
        holder.businessSkill.setText(String.valueOf(employee.getAnalysisSkill()));
        holder.serviceSkill.setText(String.valueOf(employee.getServicesSkill()));
        holder.softwareSkill.setText(String.valueOf(employee.getProgrammingSkill()));
        holder.setEmployee(employee);
    }

    /**
     * Returns size of the dataset.
     * @return size of dataset
     */
    @Override
    public int getItemCount() {
        return employees.size();
    }

    /**
     * Viewholder class for list adapter.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private EmployeeItem employeeItem;
        private boolean internal;

        private TextView name;
        private TextView softwareSkill;
        private TextView serviceSkill;
        private TextView businessSkill;

        private ViewHolder(EmployeeItem employeeItem, boolean internal) {
            super(employeeItem);
            name = employeeItem.findViewById(R.id.employeeName);
            softwareSkill = employeeItem.findViewById(R.id.softwareSkill);
            serviceSkill = employeeItem.findViewById(R.id.serviceSkill);
            businessSkill = employeeItem.findViewById(R.id.businessSkill);
            employeeItem.setOnClickListener(this);
            this.employeeItem = employeeItem;
            this.internal = internal;
        }

        /**
         * Sets the model for the employee item.
         * @param employee to set
         */
        public void setEmployee(Employee employee) {
            employeeItem.setEmployee(employee);
        }

        /**
         * on click method to add.
         * @param v view to be filled
         */
        @Override
        public void onClick(View v) {
            Game game = Game.getInstance();
            if(internal) {
                Intent employee = new Intent(v.getContext(), EmployeeActivity.class);
                employee.putExtra("employeeName", employeeItem.getEmployee().getName());
                v.getContext().startActivity(employee);
            } else {
                game.hireEmployee(employeeItem.getEmployee());
                ((EmployeesActivity) v.getContext()).doRefresh();
            }
        }
    }
}
