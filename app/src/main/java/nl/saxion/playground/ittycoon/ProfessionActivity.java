/**
 * @author Berend Hulshof, Alexandra Putuntica
 */
package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Employee;


public class ProfessionActivity extends AppCompatActivity {
    private Employee employee;

    /**
     * On create method, initialises the view.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profession);

        String employeeName = getIntent().getStringExtra("employee");
        employee = Game.getInstance().company.getEmployeeByName(employeeName);

        CardView softwareBoost = findViewById(R.id.softwareBoost);
        CardView servicesBoost = findViewById(R.id.servicesBoost);
        RelativeLayout intelligenceBoost = findViewById(R.id.inteligenceBoost);
        ImageView helpIcon = findViewById(R.id.helpIcon);

        softwareBoost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().company.removeBalance(1000);
                employee.addProgrammingSkill(10);
                Toast.makeText(getApplicationContext(), "Added 10 skill points!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        servicesBoost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().company.removeBalance(1000);
                employee.addServicesSkill(10);
                Toast.makeText(getApplicationContext(), "Added 10 skill points!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        intelligenceBoost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().company.removeBalance(1000);
                employee.addAnalysisSkill(10);
                Toast.makeText(getApplicationContext(), "Added 10 skill points!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(ProfessionActivity.this);
                ImageDialog.setTitle("Page info");
                TextView textView = new TextView(ProfessionActivity.this);
                textView.setText("Tap on one of the skill cards to add 10 points for the clicked skill card" +
                        ". But do keep in mind that this costs $1000!");
                ImageDialog.setView(textView);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }
}
