package nl.saxion.playground.ittycoon;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import android.widget.FrameLayout;

import nl.saxion.playground.ittycoon.models.Employee;

public class EmployeeItem extends FrameLayout {
    private Employee employee;

    public EmployeeItem(Context context) {
        super(context);
        inflate(getContext(), R.layout.employee_item, this);
    }

    public EmployeeItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.employee_item, this);
    }

    public EmployeeItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflate(getContext(), R.layout.employee_item, this);
    }

    /**
     * On draw method, draws on the canvas.
     * @param canvas the canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * Set employee for employee item.
     * @param employee to set
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * Gets the employee of employee item.
     * @return the employee
     */
    public Employee getEmployee() {
        return this.employee;
    }
}
