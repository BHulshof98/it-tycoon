/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.Toast;

import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Contract;

public class ContractItem extends FrameLayout {
    private Contract contract;

    public ContractItem(Context context) {
        super(context);
        inflate(getContext(), R.layout.contract_item, this);
    }

    public ContractItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.contract_item, this);
    }

    public ContractItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflate(getContext(), R.layout.contract_item, this);
    }

    /**
     * Async task that executes every second while the project hasn't been finished or failed yet.
     */
    private void startTimer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!contract.isFinished()) {
                    if (!contract.isFailed()) {
                        contract.advanceStatus();
                        ((MainActivity) getContext()).doRefreshListing();
                    } else {
                        Game.getInstance().company.removeContract(contract);
                        ((MainActivity) getContext()).doRefreshListing();
                        handler.removeCallbacks(this);
                    }
                } else {
                    if(contract.isFailed()) {
                        Game.getInstance().company.removeContract(contract);
                        ((MainActivity) getContext()).doRefreshListing();
                        handler.removeCallbacks(this);

                        Toast.makeText(getContext(),
                                contract.getDescription() + " has been failed!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        contract.executeMaintenance();
                        ((MainActivity) getContext()).doRefreshListing();
                    }
                }
            }
        }, 5000);
    }

    /**
     * On draw method, draws on the canvas.
     * @param canvas the canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * Sets the model for the contract item.
     * @param contract model to set
     */
    public void setModel(Contract contract) {
        this.contract = contract;
        startTimer();
    }

    /**
     * Returns the contract from the ContractItem
     * @return the contract
     */
    public Contract getContract() {
        return this.contract;
    }
}
