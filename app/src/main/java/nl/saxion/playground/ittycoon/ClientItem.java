package nl.saxion.playground.ittycoon;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import nl.saxion.playground.ittycoon.R;
import nl.saxion.playground.ittycoon.models.Client;

public class ClientItem extends FrameLayout {
    private Client client;

    public ClientItem(Context context) {
        super(context);
        inflate(getContext(), R.layout.client_item, this);
    }

    public ClientItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.client_item, this);
    }

    public ClientItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflate(getContext(), R.layout.client_item, this);
    }

    /**
     * Draws the view on the canvas.
     * @param canvas the canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * Sets the model for the client item.
     * @param client model to set
     */
    public void setModel(Client client) {
        this.client = client;
    }

    /**
     * Gets the client.
     * @return the client
     */
    public Client getClient() {
        return client;
    }
}
