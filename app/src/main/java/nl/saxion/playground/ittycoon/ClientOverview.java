package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nl.saxion.playground.ittycoon.adapters.ClientListAdapter;
import nl.saxion.playground.ittycoon.lib.Game;

public class ClientOverview extends AppCompatActivity {
    private Game game;

    private TextView employeeCount;
    private TextView projectCount;
    private TextView companyBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_overview);

        employeeCount = findViewById(R.id.companyEmployees);
        projectCount = findViewById(R.id.companyProjects);
        companyBalance = findViewById(R.id.companyBalance);
        RecyclerView clientList = findViewById(R.id.clientList);
        TextView companyClients = findViewById(R.id.companyClients);
        ImageView companyAvatar = findViewById(R.id.companyAvatar);
        ImageView employeeIcon = findViewById(R.id.employeeIcon);
        ImageView helpIcon = findViewById(R.id.helpIcon);

        game = Game.getInstance();
        clientList.setLayoutManager(new LinearLayoutManager(this));

        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        companyClients.setText(String.valueOf(game.company.getClients().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);

        clientList.setAdapter(new ClientListAdapter(Game.getInstance().company.getClients()));

        employeeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getApplicationContext(), EmployeesActivity.class);
                intent.putExtra("ownEmployees", true);
                startActivity(intent);
            }
        });

        employeeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getApplicationContext(), EmployeesActivity.class);
                intent.putExtra("ownEmployees", true);
                startActivity(intent);
            }
        });

        companyAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(ClientOverview.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(ClientOverview.this);
                showImage.setImageResource(R.drawable.client_overview);
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }

    /**
     * On resufme method, reloads the data in the view.
     */
    @Override
    public void onResume() {
        super.onResume();
        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);
    }
}
