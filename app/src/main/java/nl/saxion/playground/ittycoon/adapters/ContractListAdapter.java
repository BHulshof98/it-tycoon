/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import nl.saxion.playground.ittycoon.ContractActivity;
import nl.saxion.playground.ittycoon.ContractItem;

import nl.saxion.playground.ittycoon.R;
import nl.saxion.playground.ittycoon.models.Contract;

public class ContractListAdapter extends RecyclerView.Adapter<ContractListAdapter.ViewHolder>{

    private List<Contract> contracts;

    public ContractListAdapter(List<Contract> contracts) {
        this.contracts = contracts;
    }

    /**
     *  Create new views (invoked by the layout manager)
     * @param parent parent viewgroup (automatic)
     * @param viewType view type (automatic)
     * @return viewholder
     */
    @Override
    public ContractListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // no need for a LayoutInflater instance—
        // the custom view inflates itself
        ContractItem itemView = new ContractItem(parent.getContext());
        return new ViewHolder(itemView);
    }

    /**
     * Replace the contents of a view (invoked by the layout manager).
     * @param holder the viewholder
     * @param position position of item
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.contractTitle.setText(contracts.get(position).getDescription());
        holder.contractProgress.setProgress(contracts.get(position).getStatus());
        holder.setModel(contracts.get(position));
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     * @return size of dataset
     */
    @Override
    public int getItemCount() {
        return contracts.size();
    }

    /**
     * Viewholder class for list adapter.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ContractItem contractItem;
        private TextView contractTitle;
        private ProgressBar contractProgress;


        private ViewHolder(ContractItem contractItem) {
            super(contractItem);
            contractTitle = contractItem.findViewById(R.id.projectTitle);
            contractProgress = contractItem.findViewById(R.id.progressBar);
            contractItem.setOnClickListener(this);
            this.contractItem = contractItem;
        }

        /**
         * Sets the model for the contract item.
         * @param contract to set
         */
        private void setModel(Contract contract) {
            this.contractItem.setModel(contract);
        }

        /**
         * on click method to add.
         * @param v view to be filled
         */
        @Override
        public void onClick(View v) {
            // do stuff later.
            Intent intent = new Intent(v.getContext(), ContractActivity.class);
            intent.putExtra("id", contractItem.getContract().getId());
            v.getContext().startActivity(intent);
        }
    }
}
