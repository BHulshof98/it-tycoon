/**
 * @autho Sergiu Waxmann, Berend Hulshof
 */
package nl.saxion.playground.ittycoon;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import nl.saxion.playground.ittycoon.models.Contract;

public class ClientContractItem extends FrameLayout {

    private Contract contract;

    public ClientContractItem(Context context) {
        super(context);
        inflate(getContext(), R.layout.client_contract, this);
    }

    public ClientContractItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.client_contract, this);
    }

    public ClientContractItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflate(getContext(), R.layout.client_contract, this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * Sets model for client contract item.
     * @param contract to set
     */
    public void setModel(Contract contract) {
        this.contract = contract;
    }

    /**
     * Gets the client contract.
     * @return the contract
     */
    public Contract getContract() {
        return this.contract;
    }
}
