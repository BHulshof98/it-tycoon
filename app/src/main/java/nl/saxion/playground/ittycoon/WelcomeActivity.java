/**
 * @author Sergiu Waxmann
 */
package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.content.Intent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Company;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnKeyListener {

    private EditText companyNameEditText;

    /**
     * On create method, intialises the view.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ConstraintLayout backgroundConstraintLayout = findViewById(R.id.backgroundConstraintLayout);
        ImageView logoImageView = findViewById(R.id.logoImageView);
        companyNameEditText = findViewById(R.id.companyNameEditText);

        backgroundConstraintLayout.setOnClickListener(this);
        logoImageView.setOnClickListener(this);

        companyNameEditText.setOnKeyListener(this);

        Button aboutButton = findViewById(R.id.aboutButton);

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAboutDialog();
            }
        });

    }

    /**
     * Onclick method so that it hides the keyboard of the phone.
     * @param view current view
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.backgroundConstraintLayout || view.getId() == R.id.logoImageView) {
            dismissKeyboard();
        } //The user touched the background or the logo => Dismiss the keyboard
    }

    /**
     * On key method for the keyboard, so that it submits when you hit enter.
     * @param view the current view
     * @param keyCode code of the enter key
     * @param keyEvent press event
     * @return boolean if pressed;
     */
    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            submit(view);
        } //The user typed a name for the company & clicked "NEXT" on keyboard => Redirect to MainActivity
        return false;
    }

    /**
     * Submits company name and initialises the game data.
     * @param view view passed by the layout (required but not used).
     */
    public void submit(View view) {
        if (companyNameEditText.getText().toString().matches("")) { //No company name
            changeLineColorToRed(companyNameEditText);
            dismissKeyboard();
            Toast.makeText(this, "A name for the company is required.",
                    Toast.LENGTH_SHORT).show();
        } else {
            Game.getInstance().company = new Company(companyNameEditText.getText().toString());
            Game.getInstance().initDummy(this);
            redirectToMain();
        }
    }

    /**
     * Hide the phone keyboard.
     */
    private void dismissKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showAboutDialog() {
        AlertDialog.Builder ImageDialog = new AlertDialog.Builder(WelcomeActivity.this);
        ImageDialog.setTitle("Game info");
        TextView textView = new TextView(WelcomeActivity.this);
        textView.setText("The idea about this game is that you add employees and accept contracts " +
                " for clients and assign hired employees with different professions to the contracts." +
                " Contracts can fail within a certain amount of time and when you won't maintain them." +
                " Clients can be dissatisfied and leave and you will run out of money." +
                " The bigger the office the more rent you pay, and more employees means more wages to pay. " +
                " Don't go bankrupt and good luck!");
        ImageDialog.setView(textView);

        ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
            }
        });
        ImageDialog.show();
    }

    /**
     * Change line color of a field to red.
     * @param editText input to set the line color for
     */
    private void changeLineColorToRed(EditText editText) {
        editText.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
    } //Change the line color of a field to red

    private void redirectToMain() {
        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(main);
    } //Redirect to MainActivity

    /**
     * Disable Back Button
     */
    @Override
    public void onBackPressed() {
    }

}
