/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.models;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private String name;
    private Office office;
    private int balance;
    private List<Employee> employees;
    private List<Contract> contracts;
    private List<Client> clients;

    public Company(String name) {
        this.name = name;
        this.office = null;
        this.balance = 10000;
        employees = new ArrayList<>();
        contracts = new ArrayList<>();
        clients = new ArrayList<>();
    }

    /**
     * Gets the name of the company.
     * @return company name
     */
    public String getName() {
        return name;
    }

    /**
     * Get company office.
     * @return office
     */
    public Office getOffice() {
        return office;
    }

    /**
     * Gets a list of company contracts.
     * @return contract list
     */
    public List<Contract> getContracts() {
        return contracts;
    }

    /**
     * Function to get a contract by it's id.
     * @param id of contract
     * @return found contract
     */
    public Contract getContract(int id) {
        for(Contract contract: contracts) {
            if(contract.getId() == id)
                return contract;
        }
        return null;
    }

    /**
     * Gets list of company clients.
     * @return list of clients
     */
    public List<Client> getClients() {
        return clients;
    }

    /**
     * Get a client by their name.
     * @param client name
     * @return found client
     */
    public Client getClient(String client) {
        for(Client c: clients) {
            if(c.getName().equals(client))
                return c;
        }
        return null;
    }

    /**
     * Gets the list of company employees.
     * @return list of employees
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * Gets list of unassigned employees.
     * @return list of unassigned employees
     */
    public List<Employee> getUnAssigned() {
        boolean assigned;
        List<Employee> unassigned = new ArrayList<>();
        for(Employee employee: getEmployees()) {
            assigned = false;
            for(Contract contract: getContracts()) {
                if(contract.getAssignees().contains(employee)) {
                    assigned = true;
                }
            }
            if(!assigned)
                unassigned.add(employee);
        }
        return unassigned;
    }

    /**
     * Sets company office.
     * @param office to set
     */
    public void setOffice(Office office) {
        this.office = office;
    }

    /**
     * Function to hire employee.
     * @param employee to hire
     */
    public void hireEmployee(Employee employee) {
        if(this.office.canHire(this.employees.size())) {
            this.employees.add(employee);
        }
    }

    /**
     * Function to accept a contract
     * @param contract to accept
     */
    public void acceptContract(Contract contract) {
        getClient(contract.getClientName()).removeContract(contract);
        this.contracts.add(contract);
    }

    /**
     * Gets the current balance of the company.
     * @return balance of the company
     */
    public int getBalance() {
        return balance;
    }

    /**
     * Function to add balance.
     * @param balance to add
     */
    public void addBalance(int balance) {
        this.balance += balance;
    }

    /**
     * Removes balance from company.
     * @param balance to remove
     */
    public void removeBalance(int balance) {
        this.balance -= balance;
    }

    /**
     * Function to add client to the company.
     * @param client to add
     */
    public void addClient(Client client) {
        this.clients.add(client);
    }

    /**
     * Function to remove an employee from the company.
     * @param employee to remove
     */
    public void removeEmployee(Employee employee) {
        this.employees.remove(employee);
    }

    /**
     * Function to remove a contract from the company.
     * @param contract to remove
     */
    public void removeContract(Contract contract) {
        this.contracts.remove(contract);
    }

    /**
     * Function to remove a client from the company. linked contracts included.
     * @param client to remove
     */
    public void removeClient(Client client) {
        for(Contract contract: contracts) {
            if(contract.getClientName().equals(client.getName())) {
                removeContract(contract);
            }
        }
        clients.remove(client);
    }

    /**
     * Function the check client satisfactory.
     */
    public void checkSatisfiedClient() {
        for(Client client: clients) {
            if(client.getSatisfactory() <= 0) {
                removeClient(client);
            }
        }
    }

    /**
     * Get employee by name.
     * @param name of employee
     * @return found employee
     */
    public Employee getEmployeeByName(String name) {
        for (Employee employee: employees) {
            if (employee.getName().equals(name)) {
                return employee;
            }
        }
        return null;
    }

}
