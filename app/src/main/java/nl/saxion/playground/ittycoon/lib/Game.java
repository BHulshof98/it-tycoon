/**
 * @author Berend Hulshof, Sergiu Waxmann
 */
package nl.saxion.playground.ittycoon.lib;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import java.util.List;

import nl.saxion.playground.ittycoon.lib.json.JsonReader;
import nl.saxion.playground.ittycoon.models.Client;
import nl.saxion.playground.ittycoon.models.Company;
import nl.saxion.playground.ittycoon.models.Contract;
import nl.saxion.playground.ittycoon.models.Employee;
import nl.saxion.playground.ittycoon.models.Office;

public class Game {
    public Company company;
    public List<Employee> employees;
    private List<Office> offices;
    private Context context;

    private static final Game gameInstance = new Game();

    public static Game getInstance() {
        return gameInstance;
    }

    private Game() {
    }

    /**
     * Function to initialize dummy data into the game.
     * Will be replaced with a json reader and json file later on.
     */
    public void initDummy(Context context) {
        this.context = context;
        JsonReader jsonReader = new JsonReader(context);
        List<Client> clients = jsonReader.getData(Client.class);
        employees = jsonReader.getData(Employee.class);
        offices = jsonReader.getData(Office.class);
        List<Contract> contracts = jsonReader.getData(Contract.class);
        Office firstOffice = offices.get(0);
        company.setOffice(firstOffice);
        offices.remove(firstOffice);

        for(Client client : clients) {
            company.addClient(client);
        }

        List<Client> clientList = company.getClients();

        for(Contract contract: contracts) {
            for(Client client: clientList) {
                try {
                    if (contract.getClientName().equals(client.getName()))
                        client.addNewContract(contract);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        startTimer();
    }

    /**
     * Start a task for company payment.
     */
    private void startTimer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(company.getContracts().size() > 0) {
                    if (company.getBalance() > 0) {
                        int toPay = company.getOffice().getRent();
                        for (Employee employee : company.getEmployees()) {
                            toPay += employee.getSalary();
                        }
                        company.removeBalance(company.getBalance() - toPay);
                    }
                }
            }
        }, 15000);
    }

    /**
     * Hire employee to company.
     * @param employee to hire
     */
    public void hireEmployee(Employee employee) {
        if(employees.contains(employee)) {
            if(company.getOffice().canHire(company.getEmployees().size())) {
                employees.remove(employee);
                company.hireEmployee(employee);
                Toast.makeText(context, "Hired: " + employee.getName(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Could not hire employee!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Fire employee from company.
     * @param employee to fire
     */
    public void fireEmployee(Employee employee) {
        if(company.getEmployees().contains(employee)) {
            company.removeEmployee(employee);
            employees.add(employee);
        }
    }

    /**
     * Finds a client by name.
     * @param name of client
     * @return found client
     */
    public  Client getClient(String name) {
        for (Client client : company.getClients()) {
            if (client.getName().equals(name)) {
                return client;
            }
        }

        return null;
    }

    /**
     * Upgrades company office.
     */
    public void upgradeOffice() {
        if(offices.size() > 0) {
            Office office = offices.get(0);
            company.setOffice(office);
            offices.remove(office);
        }
    }

}
