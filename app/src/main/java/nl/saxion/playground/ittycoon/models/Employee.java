/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.models;

public class Employee {
    private String name;
    private int salary;
    private int programmingSkill;
    private int servicesSkill;
    private int analysisSkill;

    public Employee(String name, int salary, int programmingSkill, int servicesSkill, int analysisSkill) {
        this.name = name;
        this.salary = salary;
        this.programmingSkill = programmingSkill;
        this.servicesSkill = servicesSkill;
        this.analysisSkill = analysisSkill;
    }

    /**
     * Gets the name of the employee.
     * @return the name of the employee
     */
    public String getName() { return name; }

    /**
     * Gets the salary for the employee
     * @return salary for the employee
     */
    public int getSalary() {
        return salary;
    }

    /**
     * Increments programming skill with given amount.
     * @param amount to increment
     */
    public void addProgrammingSkill(int amount) {
        this.programmingSkill += amount;
    }

    /**
     * Increments services skill with given amount.
     * @param amount to increment
     */
    public void addServicesSkill(int amount) {
        this.servicesSkill += amount;
    }

    /**
     * Increments analysis skill with given amount.
     * @param amount to increment
     */
    public void addAnalysisSkill(int amount) {
        this.analysisSkill += amount;
    }

    /**
     * Gets the programming skill.
     * @return programming skill
     */
    public int getProgrammingSkill() { return programmingSkill; }

    /**
     * Gets the services skill.
     * @return services skill
     */
    public int getServicesSkill() {
        return servicesSkill;
    }

    /**
     * Gets the analysis skill.
     * @return analysis skill
     */
    public int getAnalysisSkill() {
        return analysisSkill;
    }

    /**
     * Check if employee is qualified to work for the given category
     * @param category of field
     * @return boolean
     */
    public boolean isQualifiedForField(Category category) {
        if(category == Category.SM) {
            return getServicesSkill() > 0;
        } else if (category == Category.SE) {
            return getProgrammingSkill() > 0;
        } else {
            return getAnalysisSkill() > 0;
        }
    }

}
