/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import nl.saxion.playground.ittycoon.ClientActivity;
import nl.saxion.playground.ittycoon.ClientItem;
import nl.saxion.playground.ittycoon.R;
import nl.saxion.playground.ittycoon.models.Client;

public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.ViewHolder> {
    private List<Client> clientList;

    public ClientListAdapter(List<Client> clientList) {
        this.clientList = clientList;
    }

    /**
     *  Create new views (invoked by the layout manager)
     * @param parent parent viewgroup (automatic)
     * @param viewType view type (automatic)
     * @return viewholder
     */
    @Override
    public ClientListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        ClientItem itemView = new ClientItem(parent.getContext());
        return new ViewHolder(itemView);
    }

    /**
     * Replace the contents of a view (invoked by the layout manager).
     * @param holder the viewholder
     * @param position position of item
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.clientName.setText(clientList.get(position).getName());
        holder.setModel(clientList.get(position));
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     * @return size of dataset
     */
    @Override
    public int getItemCount() {
        return clientList.size();
    }

    /**
     * Viewholder class for list adapter.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ClientItem clientItem;
        private TextView clientName;

        private ViewHolder(ClientItem clientItem) {
            super(clientItem);
            clientName = clientItem.findViewById(R.id.clientName);
            clientItem.setOnClickListener(this);
            this.clientItem = clientItem;
        }

        /**
         * Sets the model for the client item.
         * @param client to set
         */
        private void setModel(Client client) {
            this.clientItem.setModel(client);
        }

        /**
         * on click method to add.
         * @param v view to be filled
         */
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ClientActivity.class);
            intent.putExtra("client", clientItem.getClient().getName());
            v.getContext().startActivity(intent);
        }
    }
}
