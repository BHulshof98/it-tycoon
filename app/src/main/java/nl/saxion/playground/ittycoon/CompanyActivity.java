package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Company;
import nl.saxion.playground.ittycoon.models.Employee;

public class CompanyActivity extends AppCompatActivity {

    // I do not see the point in having a compound view here.
    private TextView companyName;
    private TextView companyOffice;
    private TextView companyRent;
    private TextView companyMoney;
    private TextView companyEmployees;
    private TextView companyWages;
    private TextView companyExpBalance;

    /**
     * On create method, initialises the view.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        companyName = findViewById(R.id.companyName);
        companyOffice = findViewById(R.id.companyOffice);
        companyRent = findViewById(R.id.companyRent);
        companyMoney = findViewById(R.id.companyMoney);
        companyEmployees = findViewById(R.id.companyEmployees);
        companyWages = findViewById(R.id.companyWages);
        companyExpBalance = findViewById(R.id.companyeExpBalance);
        ImageView helpIcon = findViewById(R.id.helpIcon);
        ImageButton companyAvatar = findViewById(R.id.companyAvatar);
        FloatingActionButton upgradeOffice = findViewById(R.id.upgradeOffice);

        companyName.setText(Game.getInstance().company.getName());
        companyOffice.setText(companyOffice.getText() + String.valueOf(Game.getInstance().company
                .getOffice().getLevel()));
        companyRent.setText(companyRent.getText() + "$" + String.valueOf(Game.getInstance().company
                .getOffice().getRent()));
        companyMoney.setText(companyMoney.getText() + "$" +  String.valueOf(Game.getInstance().company
                .getBalance()));
        companyEmployees.setText(companyEmployees.getText() + String.valueOf(Game.getInstance()
                .company.getEmployees().size()));
        companyWages.setText(companyWages.getText() + String.valueOf(calculateWages(Game.getInstance()
                .company.getEmployees())));
        companyExpBalance.setText(companyExpBalance.getText() + "$" + String.valueOf(calculateNewBalance
                (Game.getInstance().company)));

        upgradeOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().upgradeOffice();
                doRefresh();
            }
        });

        companyAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(CompanyActivity.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(CompanyActivity.this);
                showImage.setImageResource(R.drawable.company);
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }

    /**
     * Calculate wages of all employees.
     * @param employees all company employees
     * @return total wages of company employees
     */
    private int calculateWages(List<Employee> employees) {
        int total = 0;
        for(Employee employee: employees) {
            total += employee.getSalary();
        }
        return total;
    }

    /**
     * Calculate the new balance for the company.
     * @param company current company
     * @return calculated balance
     */
    private int calculateNewBalance(Company company) {
        return company.getBalance() - (company.getOffice().getRent() + calculateWages(company.getEmployees()));
    }

    /**
     * Refresh view with new data.
     */
    public void doRefresh() {
        companyName.setText(Game.getInstance().company.getName());
        companyOffice.setText("Office: " + String.valueOf(Game.getInstance().company.getOffice()
                .getLevel()));
        companyRent.setText("Rent: $" + String.valueOf(Game.getInstance().company.getOffice().getRent()));
        companyMoney.setText("Money: $" + String.valueOf(Game.getInstance().company.getBalance()));
        companyEmployees.setText("Employees: " + String.valueOf(Game.getInstance().company.
                getEmployees().size()));
        companyWages.setText("Wages: " + String.valueOf(calculateWages(Game.getInstance().company
                .getEmployees())));
        companyExpBalance.setText("Exp. Balance: $" + String.valueOf(calculateNewBalance(Game
                .getInstance().company)));

    }
}
