package nl.saxion.playground.ittycoon.lib.json;

import org.json.JSONException;
import org.json.JSONObject;

import nl.saxion.playground.ittycoon.models.Category;
import nl.saxion.playground.ittycoon.models.Client;
import nl.saxion.playground.ittycoon.models.Company;
import nl.saxion.playground.ittycoon.models.Contract;
import nl.saxion.playground.ittycoon.models.Employee;
import nl.saxion.playground.ittycoon.models.Office;

/**
 * @author berend hulshof
 * Converter class to help with the conversion of JSON to objects.
 * There is a version which does it vice versa in my project for Android Programming Q1.3.
 */
public class JsonConvert {
    /**
     * Convert json object to a festival object.
     * @param jobj object to convert.
     * @return Festival object.
     */
    public Client clientFromJson(JSONObject jobj) {
        try {
            return new Client(jobj.getString("name"));
        } catch (JSONException jex) {
            jex.printStackTrace();
            return null;
        }
    }

    /**
     * Convert a json object to a contract object.
     * @param jobj json object to convert
     * @return contract object
     */
    public Contract contractFromJson(JSONObject jobj) {
        try {
            Category category;
            String jsonCategory = jobj.getString("category");
            switch (jsonCategory) {
                case "BI":
                    category = Category.BI;
                    break;
                case "SE":
                    category = Category.SE;
                    break;
                case "SM":
                    category = Category.SM;
                    break;
                    default:
                        category = Category.BI;
                        break;
            }
            return new Contract(
                    Integer.parseInt(jobj.getString("id")),
                    jobj.getString("client"),
                    jobj.getString("description"),
                    Integer.parseInt(jobj.getString("time")),
                    Integer.parseInt(jobj.getString("profit")),
                    category);
        } catch (JSONException jex) {
            jex.printStackTrace();
            return null;
        }
    }

    /**
     * Convert a json object to a company object.
     * @param jobj json object to convert
     * @return company object
     */
    public Company companyFromJson(JSONObject jobj) {
        try {
            return new Company(
                    jobj.getString("name")
            );
        } catch (JSONException jex) {
            jex.printStackTrace();
            return null;
        }
    }

    /**
     * Convert a json object to an employee object.
     * @param jobj json object to convert
     * @return employee object
     */
    public Employee employeeFromJson(JSONObject jobj) {
        try {
            return new Employee(
                    jobj.getString("name"),
                    Integer.parseInt(jobj.getString("salary")),
                    Integer.parseInt(jobj.getString("programmingSkill")),
                    Integer.parseInt(jobj.getString("servicesSkill")),
                    Integer.parseInt(jobj.getString("analysisSkill"))
            );
        } catch (JSONException jex) {
            jex.printStackTrace();
            return null;
        }
    }

    /**
     * Convert a json object to an office object.
     * @param jobj json object to convert
     * @return office object
     */
    public Office officeFromJson(JSONObject jobj) {
        try {
            return new Office(
                    Integer.parseInt(jobj.getString("rent")),
                    Integer.parseInt(jobj.getString("workSpots")),
                    Integer.parseInt(jobj.getString("level")));
        } catch (JSONException jex) {
            jex.printStackTrace();
            return null;
        }
    }
}
