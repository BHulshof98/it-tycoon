/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.models;

import java.util.ArrayList;
import java.util.List;

import nl.saxion.playground.ittycoon.lib.Game;

public class Contract {
    private int id;
    private String description;
    private String client;
    private int time;
    private int status;
    private int profit;
    private int maintenanceCost;
    private boolean finished;
    private boolean failed;
    private Category category;
    private List<Employee> assignees;

    public Contract(int id, String client, String description, int time, int profit, Category category) {
        this.id = id;
        this.client = client;
        this.description = description;
        this.time = time;
        this.status = 0;
        this.profit = profit;
        this.category = category;
        this.finished = false;
        this.failed = false;
        this.assignees = new ArrayList<>();
        maintenanceCost = 1000;
    }

    /**
     * Returns contract id.
     * @return id of contract
     */
    public int getId() {
        return id;
    }

    /**
     * Gets category of contract.
     * @return category of contract
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Gets description of contract.
     * @return description of contract
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets status of contract.
     * @return status of contract
     */
    public int getStatus() {
        return status;
    }

    /**
     * Gets list of assignees assigned to the contract.
     * @return list of assignees
     */
    public List<Employee> getAssignees() {
        return assignees;
    }

    /**
     * Gets the profit of the contract.
     * @return profit of contract
     */
    public int getProfit() {
        return profit;
    }

    /**
     * This function assigns an employee to the contract.
     * @param employee to assign
     */
    public void assign(Employee employee) {
        if(!this.assignees.contains(employee)) {
            if(employee.isQualifiedForField(this.category)) {
                this.assignees.add(employee);
            }
        }
    }

    /**
     * Execute maintenance logic.
     */
    public void executeMaintenance() {
        int advance = getAdvance();
        int costs = maintenanceCost % status;
        Game.getInstance().company.addBalance(costs);
        if(status != 0) {
            if (advance == 0) {
                status -= 25;
            } else {
                if (status >= 1000) {
                    setStatus(1000);
                } else {
                    status += advance;
                }
            }
        } else {
            setFailed();
        }
    }

    /**
     * this function removes an assignee from the contract.
     * @param employee the assignee to remove
     */
    public void removeAssignee(Employee employee) {
        if(this.assignees.contains(employee)) {
            this.assignees.remove(employee);
        }
    }

    /**
     * Sets the finished state for the contract.
     * @param finished boolean
     */
    private void setFinished(boolean finished) {
        this.finished = finished;
        Game.getInstance().company.addBalance(profit);
    }

    /**
     * Gets the client name.
     * @return name of client
     */
    public String getClientName() {
        return client;
    }

    /**
     * Checks if contract is finished or not.
     * @return finished status for contract
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Sets the status for the contract.
     * This will be used for either the status limit check or a pre-made status.
     * @param status of the project
     */
    private void setStatus(int status) {
        this.status = status;
        Game.getInstance().company.getClient(client).satisfy(50);
    }

    /**
     * Function to advance the contract's status.
     */
    public void advanceStatus() {
        int adv = getAdvance();
        this.status += adv;
        time -= 1;
        if(time <= 0) {
            setFailed();
        }
        if(!isFailed()) {
            if (this.status >= 1000) {
                setStatus(1000);
                setFinished(true);
            }
        } else {
            failed = true;
        }
    }

    /**
     * Sets the fail condition for the contract.
     */
    public void setFailed() {
        this.failed = true;
        Game.getInstance().company.getClient(client).dissatify(25);
        Game.getInstance().company.checkSatisfiedClient();
    }

    /**
     * Gets the fail condition of the contract.
     * @return fail condition
     */
    public boolean isFailed() {
        return failed;
    }

    /**
     * Gets the number for advancement through a category based calculation.
     * @return number to increment on the status
     */
    private int getAdvance() {
        int advance = 0;
        for(Employee employee: assignees) {
            if(category == Category.BI) {
                advance += employee.getAnalysisSkill();
            } else if (category == Category.SE) {
                advance += employee.getProgrammingSkill();
            } else if (category == Category.SM) {
                advance += employee.getServicesSkill();
            }
        }
        return advance;
    }

}
