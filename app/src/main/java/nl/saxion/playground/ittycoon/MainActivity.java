package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nl.saxion.playground.ittycoon.adapters.ContractListAdapter;
import nl.saxion.playground.ittycoon.lib.*;
import nl.saxion.playground.ittycoon.models.Contract;

public class MainActivity extends AppCompatActivity {
    private Game game;
    private TextView employeeCount;
    private TextView projectCount;
    private TextView companyBalance;
    private RecyclerView progressList;
    private RecyclerView maintenanceList;
    private List<Contract> done;
    private List<Contract> ongoing;

    /**
     * On create method, initialises the view.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        game = Game.getInstance();

        done = new ArrayList<>();
        ongoing = new ArrayList<>();

        employeeCount = findViewById(R.id.companyEmployees);
        projectCount = findViewById(R.id.companyProjects);
        companyBalance = findViewById(R.id.companyBalance);
        progressList = findViewById(R.id.progressList);
        maintenanceList = findViewById(R.id.maintenanceList);

        ImageView companyAvatar = findViewById(R.id.companyAvatar);
        TextView companyClients = findViewById(R.id.companyClients);
        ImageView employeeIcon = findViewById(R.id.employeeIcon);
        ImageView clientIcon = findViewById(R.id.clientsIcon);
        ImageView helpIcon = findViewById(R.id.helpIcon);

        progressList.setLayoutManager(new LinearLayoutManager(this));
        maintenanceList.setLayoutManager(new LinearLayoutManager(this));

        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        companyClients.setText(String.valueOf(game.company.getClients().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);
        List<ContractListAdapter> contractListAdapters = getContractAdapters(ongoing, done);
        progressList.setAdapter(contractListAdapters.get(0));
        maintenanceList.setAdapter(contractListAdapters.get(1));

        for(Contract contract: game.company.getContracts()) {
            if(contract.isFinished()) {
                done.add(contract);
            } else {
                ongoing.add(contract);
            }
        }

        employeeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getApplicationContext(), EmployeesActivity.class);
                intent.putExtra("ownEmployees", true);
                startActivity(intent);
            }
        });

        employeeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getApplicationContext(), EmployeesActivity.class);
                intent.putExtra("ownEmployees", true);
                startActivity(intent);
            }
        });

        companyAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CompanyActivity.class);
                startActivity(intent);
            }
        });

        companyClients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ClientOverview.class);
                startActivity(intent);
            }
        });

        clientIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ClientOverview.class);
                startActivity(intent);
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(MainActivity.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(MainActivity.this);
                showImage.setImageResource(R.drawable.main);
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }

    /**
     * On resume method, reloads game data;
     */
    @Override
    public void onResume() {

        if (game.company.getBalance() <= 0) {
            redirectToEndGame();
        }

        super.onResume();
        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);

        done.clear();
        ongoing.clear();

        for(Contract contract: game.company.getContracts()) {
            if(!contract.isFailed()) {
                if (contract.isFinished()) {
                    done.add(contract);
                } else {
                    ongoing.add(contract);
                }
            }
        }

        progressList.post(new Runnable() {
            @Override
            public void run() {
                progressList.getAdapter().notifyDataSetChanged();
            }
        });

        maintenanceList.post(new Runnable() {
            @Override
            public void run() {
                maintenanceList.getAdapter().notifyDataSetChanged();
            }
        });
    }

    /**
     * Refresh recycler views.
     */
    public void doRefreshListing() {

        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);

        done.clear();
        ongoing.clear();

        for(Contract contract: game.company.getContracts()) {
            if(!contract.isFailed()) {
                if (contract.isFinished()) {
                    done.add(contract);
                } else {
                    ongoing.add(contract);
                }
            } else {
                System.out.println("Failed project");
            }
        }

        progressList.post(new Runnable() {
            @Override
            public void run() {
                progressList.getAdapter().notifyDataSetChanged();
            }
        });

        maintenanceList.post(new Runnable() {
            @Override
            public void run() {
                maintenanceList.getAdapter().notifyDataSetChanged();
            }
        });
    }

    /**
     * Gets list adapters for the two recycler views.
     * @param ongoing list of ongoing contracts.
     * @param done list of finished contracts.
     * @return list of two list adapters.
     */
    private List<ContractListAdapter> getContractAdapters(List<Contract> ongoing, List<Contract> done) {
        List<ContractListAdapter> adapterList = new ArrayList<>();
        ContractListAdapter contractOngoingAdapter = new ContractListAdapter(ongoing);
        ContractListAdapter contractDoneAdapter = new ContractListAdapter(done);
        contractOngoingAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });
        contractDoneAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });
        adapterList.add(contractOngoingAdapter);
        adapterList.add(contractDoneAdapter);
        return adapterList;
    }

    /**
     * Redirect to end game.
     */
    private void redirectToEndGame() {
        Intent endGame = new Intent(getApplicationContext(), EndGameActivity.class);
        startActivity(endGame);
    }

}
