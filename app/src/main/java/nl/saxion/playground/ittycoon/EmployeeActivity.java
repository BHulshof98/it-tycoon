/**
 * @author Sergiu Waxmann
 */
package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Employee;

public class EmployeeActivity extends AppCompatActivity {
    private Employee employee;

    private TextView seSkillPoints;
    private TextView smSkillPoints;
    private TextView biSkillPoints;

    /**
     * On create method, initialises the activity data.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);

        seSkillPoints = findViewById(R.id.seSkillPointsTextView);
        smSkillPoints = findViewById(R.id.smSkillPointsTextView);
        biSkillPoints = findViewById(R.id.biSkillPointsTextView);
        TextView employeeName = findViewById(R.id.employeeNameTextView);
        Button addProfessionButton = findViewById(R.id.addProfessionButton);
        Button fireEmployeeButton = findViewById(R.id.fireEmployeeButton);
        ImageView helpIcon = findViewById(R.id.helpIcon);

        employeeName.setText(getIntent().getStringExtra("employeeName"));

        employee = Game.getInstance().company.getEmployeeByName(getIntent()
                .getStringExtra("employeeName"));

        seSkillPoints.setText(String.valueOf(Game.getInstance().company.getEmployeeByName(getIntent()
                .getStringExtra("employeeName")).getProgrammingSkill()));
        smSkillPoints.setText(String.valueOf(Game.getInstance().company.getEmployeeByName(getIntent()
                .getStringExtra("employeeName")).getServicesSkill()));
        biSkillPoints.setText(String.valueOf(Game.getInstance().company.getEmployeeByName(getIntent()
                .getStringExtra("employeeName")).getAnalysisSkill()));

        addProfessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfessionActivity.class);
                intent.putExtra("employee", employee.getName());
                intent.putExtra("overview", false);
                startActivity(intent);
            }
        });

        fireEmployeeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().fireEmployee(employee);
                finish();
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(EmployeeActivity.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(EmployeeActivity.this);
                showImage.setImageResource(R.drawable.employee);
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }

    /**
     * On resume method, reloads activity data.
     */
    @Override
    public void onResume() {
        super.onResume();
        seSkillPoints.setText(String.valueOf(Game.getInstance()
                .company.getEmployeeByName(getIntent().getStringExtra("employeeName"))
                .getProgrammingSkill()));
        smSkillPoints.setText(String.valueOf(Game.getInstance()
                .company.getEmployeeByName(getIntent().getStringExtra("employeeName"))
                .getServicesSkill()));
        biSkillPoints.setText(String.valueOf(Game.getInstance()
                .company.getEmployeeByName(getIntent().getStringExtra("employeeName"))
                .getAnalysisSkill()));
    }
}
