package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nl.saxion.playground.ittycoon.adapters.EmployeeListAdapter;
import nl.saxion.playground.ittycoon.lib.Game;

public class EmployeesActivity extends AppCompatActivity {
    private Game game;
    private TextView employeeCount;
    private TextView projectCount;
    private TextView companyBalance;
    private RecyclerView employeeList;

    private boolean ownEmployees;

    /**
     * On create method,
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);

        employeeCount = findViewById(R.id.companyEmployees);
        projectCount = findViewById(R.id.companyProjects);
        companyBalance = findViewById(R.id.companyBalance);
        employeeList = findViewById(R.id.employeeList);
        TextView companyClients = findViewById(R.id.companyClients);
        ImageView companyAvatar = findViewById(R.id.companyAvatar);
        FloatingActionButton hireEmlpoyees = findViewById(R.id.hireEmployees);
        ImageView clientIcon = findViewById(R.id.clientsIcon);
        ImageView helpIcon = findViewById(R.id.helpIcon);

        employeeList.setLayoutManager(new LinearLayoutManager(this));

        game = Game.getInstance();

        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        companyClients.setText(String.valueOf(game.company.getClients().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);
        ownEmployees = getIntent().getBooleanExtra("ownEmployees", false);

        if(ownEmployees) {
            employeeList.setAdapter(new EmployeeListAdapter(game.company.getEmployees(), ownEmployees));

            hireEmlpoyees.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), EmployeesActivity.class);
                    intent.putExtra("ownEmployees", false);
                    startActivity(intent);
                }
            });
        } else {
            hireEmlpoyees.hide();
            employeeList.setAdapter(new EmployeeListAdapter(game.employees, ownEmployees));
        }

        companyClients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ClientOverview.class);
                startActivity(intent);
            }
        });


        clientIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ClientOverview.class);
                startActivity(intent);
            }
        });

        companyAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        // onclick
        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(EmployeesActivity.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(EmployeesActivity.this);
                if(ownEmployees) {
                    showImage.setImageResource(R.drawable.employees);
                } else {
                    showImage.setImageResource(R.drawable.hire_employee);
                }
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });
    }

    /**
     * On resume method, reloads the activity data.
     */
    @Override
    public void onResume() {
        super.onResume();
        if (game.company.getBalance() <= 0) {
            redirectToEndGame();
        }
        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);
        employeeList.getAdapter().notifyDataSetChanged();
    }

    /**
     * Refreshes the data for the view.
     */
    public void doRefresh() {
        if (game.company.getBalance() <= 0) {
            redirectToEndGame();
        }
        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);
        employeeList.getAdapter().notifyDataSetChanged();
    }

    /**
     * Redirect to end game activity.
     */
    private void redirectToEndGame() {
        Intent endGame = new Intent(getApplicationContext(), EndGameActivity.class);
        startActivity(endGame);
    }
}
