package nl.saxion.playground.ittycoon;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import nl.saxion.playground.ittycoon.adapters.EmployeeListAdapter;
import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Category;
import nl.saxion.playground.ittycoon.models.Contract;
import nl.saxion.playground.ittycoon.models.Employee;

public class ContractActivity extends AppCompatActivity {
    private Game game;
    private Contract contract;

    private TextView employeeCount;
    private TextView projectCount;
    private TextView companyBalance;
    private ProgressBar contractProgress;
    private RecyclerView employeeList;

    /**
     * On create method, creates and initialises the view.
     * @param savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract);

        employeeCount = findViewById(R.id.companyEmployees);
        projectCount = findViewById(R.id.companyProjects);
        companyBalance = findViewById(R.id.companyName);
        employeeList = findViewById(R.id.employeeList);

        ImageView helpIcon = findViewById(R.id.helpIcon);
        TextView contractName = findViewById(R.id.contractName);
        TextView contractCategory = findViewById(R.id.contractCategory);
        TextView companyClients = findViewById(R.id.companyClients);
        contractProgress = findViewById(R.id.contractProgress);
        FloatingActionButton addAssignee = findViewById(R.id.addAssignee);
        FloatingActionButton removeAssignee = findViewById(R.id.removeAssignee);
        ImageButton companyAvatar = findViewById(R.id.companyAvatar);
        ImageView employeeIcon = findViewById(R.id.employeeIcon);
        ImageView clientIcon = findViewById(R.id.clientsIcon);

        employeeList.setLayoutManager(new LinearLayoutManager(this));

        game = Game.getInstance();

        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        companyClients.setText(String.valueOf(game.company.getClients().size()));
        companyBalance.setText(String.valueOf(game.company.getBalance()));

        int id = getIntent().getIntExtra("id", 0);
        this.contract = Game.getInstance().company.getContract(id);

        contractName.setText(contract.getDescription());

        if(contract.getCategory() == Category.SM) {
            contractCategory.setText("Service Management");
        } else if(contract.getCategory() == Category.SE) {
            contractCategory.setText("Software Engineering");
        } else {
            contractCategory.setText("Business Inteligence");
        }

        contractProgress.setProgress(contract.getStatus());

        employeeList.setAdapter(new EmployeeListAdapter(contract.getAssignees(), true));

        addAssignee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAssignDialog();
            }
        });

        removeAssignee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRemoveDialog();
            }
        });

        companyClients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ClientOverview.class);
                startActivity(intent);
            }
        });

        clientIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ClientOverview.class);
                startActivity(intent);
            }
        });

        companyAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        employeeCount.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EmployeesActivity.class);
                startActivity(intent);
            }
        });

        employeeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EmployeesActivity.class);
                startActivity(intent);
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ImageDialog = new AlertDialog.Builder(ContractActivity.this);
                ImageDialog.setTitle("Page info");
                ImageView showImage = new ImageView(ContractActivity.this);
                showImage.setImageResource(R.drawable.contract);
                ImageDialog.setView(showImage);

                ImageDialog.setNegativeButton("ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                    }
                });
                ImageDialog.show();
            }
        });

        startTimer();
    }

    /**
     * Show assign dialog to pick an assignee.
     */
    private void showAssignDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContractActivity.this);
        builder.setTitle("Choose an assignee");

        final List<Employee> unassigned = Game.getInstance().company.getUnAssigned();
        List<String> names = new ArrayList<>();

        for(Employee employee : unassigned) {
            names.add(employee.getName());
        }

        String[] unassignedNames = names.toArray(new String[0]);
        builder.setItems(unassignedNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contract.assign(unassigned.get(which));
                Toast.makeText(ContractActivity.this,
                        "Assigned employee!",
                        Toast.LENGTH_SHORT).show();
                employeeList.getAdapter().notifyDataSetChanged();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * remove assignee
     */
    private void showRemoveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContractActivity.this);
        builder.setTitle("Choose an assignee");

        final List<Employee> assignees = contract.getAssignees();
        List<String> names = new ArrayList<>();

        for(Employee employee : assignees) {
            names.add(employee.getName());
        }

        final String[] assigned = names.toArray(new String[0]);
        builder.setItems(assigned, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contract.removeAssignee(assignees.get(which));
                employeeList.getAdapter().notifyDataSetChanged();
                Toast.makeText(ContractActivity.this,
                        "Removed employee!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * On resume method, reloads activity data.
     */
    @Override
    public void onResume() {
        super.onResume();
        if (game.company.getBalance() <= 0) {
            redirectToEndGame();
        }
        employeeCount.setText(String.valueOf(game.company.getEmployees().size()));
        projectCount.setText(String.valueOf(game.company.getContracts().size()));
        String balance = "$" + game.company.getBalance();
        companyBalance.setText(balance);

        startTimer();
    }


    /**
     * Async task that executes every second while the project hasn't been finished or failed yet.
     */
    private void startTimer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!contract.isFinished()) {
                    if (!contract.isFailed()) {
                        contract.advanceStatus();
                        doRefresh();
                    } else {
                        Game.getInstance().company.removeContract(contract);
                        handler.removeCallbacks(this);
                        doRefresh();
                    }
                } else {
                    if(contract.isFailed()) {
                        Game.getInstance().company.removeContract(contract);
                        handler.removeCallbacks(this);

                        Toast.makeText(ContractActivity.this,
                                contract.getDescription() + " has been failed!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        contract.executeMaintenance();
                        doRefresh();
                    }
                }
            }
        }, 5000);
    }

    /**
     * Refreshes the contract status.
     */
    public void doRefresh() {
        contractProgress.setProgress(contract.getStatus());
    }

    /**
     * Redirect to fail screen.
     */
    private void redirectToEndGame() {
        Intent endGame = new Intent(getApplicationContext(), EndGameActivity.class);
        startActivity(endGame);
    }
}
