/**
 * @author Sergiu Waxmann, Berend Hulshof
 */
package nl.saxion.playground.ittycoon.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import nl.saxion.playground.ittycoon.ClientActivity;
import nl.saxion.playground.ittycoon.ClientContractItem;

import nl.saxion.playground.ittycoon.R;
import nl.saxion.playground.ittycoon.lib.Game;
import nl.saxion.playground.ittycoon.models.Category;
import nl.saxion.playground.ittycoon.models.Contract;

public class ClientContractListAdapter extends RecyclerView.Adapter<ClientContractListAdapter.ViewHolder>{

    private List<Contract> contracts;

    public ClientContractListAdapter(List<Contract> contracts) {
        this.contracts = contracts;
    }

    /**
     *  Create new views (invoked by the layout manager)
     * @param parent parent viewgroup (automatic)
     * @param viewType view type (automatic)
     * @return viewholder
     */
    @Override
    public ClientContractListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // no need for a LayoutInflater instance—
        // the custom view inflates itself
        ClientContractItem itemView = new ClientContractItem(parent.getContext());
        return new ViewHolder(itemView);
    }

    /**
     * Replace the contents of a view (invoked by the layout manager).
     * @param holder the viewholder
     * @param position position of item
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.contractTitle.setText(contracts.get(position).getDescription());
        holder.contractProfit.setText("$" + String.valueOf(contracts.get(position).getProfit()));
        if (contracts.get(position).getCategory() == Category.SE) {
            holder.contractCategory.setText("Software Engineering");
        } else if (contracts.get(position).getCategory() == Category.SM) {
            holder.contractCategory.setText("Service Management");
        } else {
            holder.contractCategory.setText("Business Inteligence");
        }
        holder.setModel(contracts.get(position));
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     * @return size of dataset
     */
    @Override
    public int getItemCount() {
        return contracts.size();
    }

    /**
     * Viewholder class for list adapter.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ClientContractItem clientContractItem;
        private TextView contractTitle;
        private TextView contractCategory;
        private TextView contractProfit;

        private ViewHolder(ClientContractItem clientContractItem) {
            super(clientContractItem);
            contractTitle = clientContractItem.findViewById(R.id.projectTitle);
            contractCategory = clientContractItem.findViewById(R.id.projectCategory);
            contractProfit = clientContractItem.findViewById(R.id.projectProfit);

            clientContractItem.setOnClickListener(this);
            this.clientContractItem = clientContractItem;
        }

        /**
         * Sets the model for the contract item.
         * @param contract to set
         */
        private void setModel(Contract contract) {
            this.clientContractItem.setModel(contract);
        }

        /**
         * on click method to add.
         * @param v view to be filled
         */
        @Override
        public void onClick(View v) {
            Game.getInstance().company.acceptContract(clientContractItem.getContract());
            Toast.makeText(v.getContext(), "Contract Accepted!", Toast.LENGTH_SHORT).show();
            ((ClientActivity) v.getContext()).doRefresh();
        }
    }
}
