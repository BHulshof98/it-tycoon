/**
 * @author Berend Hulshof
 */
package nl.saxion.playground.ittycoon.models;

// all types of category.
public enum Category {
    SE,
    SM,
    BI
}
