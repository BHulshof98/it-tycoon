## Functional design

`IT Tycoon` is a simulation game about an IT company. The user is playing the role of its owner and has the goal to grow up his company by: hiring  more employees, signing contracts, topping up the employees skills, buying several offices, etc.


### Splash Screen

This is the first screen, that is opened when tapping the app icon. This is the logo/title of the game, that will only stay for several seconds and then change to the next activity.

![](image-1.jpg)

- A. The logo/title .



### Welcome Screen

This screen will welcome the player, where he has to enter the name of his company and then change to the [Maine Activity](#main-screen).

![](image-2.jpg)

- A. The logo/title .
- B. An EditText View to enter the name of your own company.
- C. The play button. Tapping it will start [Main Screen](#main-screen). A subtle animation to make it more attractive.



### Main Screen

This section shows the overview of the state of your company.

![](image-3.jpg)

- A. Company logo. When pressing it will start a new activity - [Company Information](#company-information). 
- B. Money. The amount of money the company earns in total, including the projects and signed contracts.
- C. Employees. It will show the number of workers that are hired at the company.
- D. Contracts. The amount of contracts that have been signed.
- E. Progress. A list of the new projects that has a progress bar each, which describes at what stage of implementation it is. Tapping on a project will start a [new activity](#project-information).
- F. Maintenance. A list of the projects that already have been done and their progress bar, that determine the state of the maintenance. If the progress bar is low, employees should be assigned to fix the problems. When clicking on a project it will show you the information about that specific project in [Contract Information].(#contract-information)



### Contract Information
The information about contracts. It is opened while choosing a contract from the list.

![](image-4.jpg)

- A. The header with the company logo and name.
- B. Employees and contracts. As in the [Main Screen](#main-screen).
- C. The contract name and number 
- D. Progress bar. If it is a new project, then the progress bar shows the level of its implementation, however if it is a completed project it will show the status of the maintenance process.
- E. Category. It will show one of the category type: Software Engineering, Services or Business Intelligence.
- F. Employees. Number of employees assigned for that project and their details.



### Company Information

This section has every detail about your company. It is opened while tapping the company logo or its name in the main activity.

![](image-6.jpg)

- A. Header. Is the same in every activity.
- B. Office. The level of the offices that the player had bought.
- C. Rent. The cost of the office rent.
- D. Money. The current amount of money. 
- E. Employees. Like point ['D'](#main-screen) in Main Screen.
- F. Wage. The amount of wage.
- G. Expected Balance. The money that you will have after paying the costs.



### Employee Overview

A list with all the employees that work for the company. It is accessed while clicking the employee button from the header of each page.

![](image-9.jpg)

- A. Header. Is the same in every activity.
- B. Employee list. The employees that are working in the company with their name and skill set.
- C. Floating button. This is a floating button for the employee overview page.



### Employee Information

The information about employees. It is accessed while choosing an employee from the [Employee Overview](#employee-overview). Each employee has 3 skill sets: SE(Software Engineering),SM(Service Management) and BI(Business Intelligence). Every employee has a wedge, can accept specific contracts, can hire new employees, even more, the player can add a certificate and profession to that specific employee. 

![](image-7.jpg)

- A. Name. Employee's name.
- B. Skill set. The level of his abilities. Each skill set can reach a maximum of 100.
- C. Overview of professions. This button will open an overview with the professions.
- D. Add profession. The player can add certificate to a specific employee, about a programming language or a profession.
- E. Fire employee. Player can chose to fire or not that specific employee.



### Profession Overview

The information about the profession that an employee can have. It is accessed while clicking on the [Profession Overview](#profession-overview) page. 

![](image-8.jpg)

- A. Title. Profession Overview text.
- B. The list of profession in a card view.



### Clients Overview

The overview list of the clients. It is accessed while clicking on the clients overview button in the header. 

![](image-10.jpg)

- A. Header. Is the same in every activity.
- B. Title. Clients Overview TextView.
- C. Clients. List of the client where you can choose one to see more information.



### Client Information

Here you can see the information about the client. It is accessed  when pressing on one of th clients in the [Clients Overview](#clients-overview) page.

![](image-11.jpg)

- A. Client's name TextView.
- B. Contracts. List of the contracts that specific client offers to the company to sign.
