## Werkafspraken / Code of Conduct

## 1. Organization
**Member** | **function**
--- | ---
Berend Hulshof | **Scrum Master, Developer, Support**
Sergiu Waxmann | **Developer**
Adnrei Terus | **Developer**
Alexandra Putuntica | **Developer**

## 2. Puropes of the Agreement
Berend Hulshof, Sergiu Waxmann, Andrei Terus and Alexandra Putuntica have agreed to
work together in a team for Project Playground. 

This agreement describes their
understandings and commitments to this collaborative effort. 

## 3. Scope and Duration
The team’s purpose is to create a 2D simulation game in Android Studio, where the player runs an IT company and has to manage that the player does not go bankrupt. 

This agreement will guide the collaboration for the period of one quarter. 

The purpose and duration of the collaboration may be amended and/or extended through the joint agreement of all members.

## 4. Decision-making Structure & Authority
All significant decisions regarding the collaboration will require agreement by all four
collaborative partners, the final decision will be made by the Scrum Master, so that he can convert this into a backlog item and assign himself or any of the other team members. 

Significant decisions will include decisions regarding
documentation, improvements and additions for the final product. 

## 5. Resource Commitment of the Collaboration
Each has agreed to commit resources to the collaboration. All members will contribute:
- the time and effort required for consistent representation of and participation
- careful attention to risk assessment
- to submit the work(assigned scrum cards) before the deadline

## 6. Definition of Done
An item will be defined done when it has been carfully tested by a developer which has not been assigned to that card before testing, the reason for this is that the newly assigned developer is more likely to go through it as a real user.

When the item has been succesfully tested by the newly assigned developer, the scrum card will be defined as done.

When every card, which has been assigned to the current milestone, has been placed in done the progress made by all the teammembers will be merged into the master branch. This means that there will me a new itteration of the game.

## 7. Meetings
The meetings will be held each Friday from 2:00 PM in Saxion café or on Discord.
Team members are expected to be present unless they have a significant reason to skip the
meeting.

The present members of the team will decide if the reason is genuine. The absent
member must inform the team leader in advance. The meetings schedule can be rearranged
after agreement. Arriving more than 20 minutes late will be fined - the member must bring
a beer for the hole team.

During the meeting, the team will discuss what work they have made and what
problems they encountered. They will solve the problems working together and after that
will assign individual tasks for the following week. Except Fridays, each member will work
on their own and, if they need to communicate during the week, they will arrange online
meetings on Discord.

## 8. Weekly hours
For each teammember the minimum required hours will be 8.
