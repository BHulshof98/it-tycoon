## IT Tycoon library

IT Tycoon is a simulation game where the player runs an IT company. The goal is to accept contracts and finish these in time,
next to this you also need to keep your balance above 0 or you will lose the game.

### About this document

This document is written in Markdown using PlantUML for diagrams.
This document will explain the working of the game


### Features and scope

The classes all hold information about the game mechanics, as of the 0.1 milestone these should be:

- Get company info and display this.
- Manage employees for the company.
- Manage contracts for the company. 
- base logic fully provided and supported (Yet to implement most of it).
- Basic UI elements.

To see what hes yet to come, you should check the issues page.
Or contact Berend Hulshof (462753@student.saxion.nl) to add you to the team's private trello board.


### Overview

The game will be represented with the Game class, which is a singleton which holds all the game's data. We know that it is bad practice in most cases but we found this an easy solution.

The rest will be explained in the class diagram below.

### Class diagram

This UML class diagram shows the use of the base library with a simple example game. Many details have been intentionally left out.

```plantuml

@startuml

title IT Tycoon - Class Diagram

Game *-- "1" Company
Game *-- "*" Client
Game *-- "*" Employee
Company *-- "1..*" Client
Company *-- "1..*" Contract
Company o-- "0..*" Employee
Company *-- "1" Office
Client *-- "1..10" Contract
Contract *-- "1" Category
Contract *-- "0..3" Employee
Employee *-- "1..3" Profession

MainActivity ..> Game : uses
MainActivity o-- "0..*" ContractItem: RecyclerView
CompanyActivity ..> Game : uses
ContractActivity ..> Game : uses
EmployeeActivity ..> Game : uses
EmployeesActivity ..> Game : uses
EmployeesActivity o-- "0..*" EmlpoyeeItem: RecyclerView

Android.Activity <|-- MainActivity
Android.Activity <|-- CompanyActivity
Android.Activity <|-- ContractActivity
Android.Activity <|-- EmployeeActivity
Android.Activity <|-- EmployeesActivity

Android.FrameLayout <|-- EmployeeItem
Android.FrameLayout <|-- ContractItem
Android.RecyclerView.ViewHolder <|-- ContractListAdapter.ViewHolder
Android.RecyclerView.ViewHolder <|-- EmployeeListAdapter.ViewHolder

ContractListAdapter.ViewHolder *-- "1" ContractItem
EmployeeListAdapter.ViewHolder *-- "1" EmployeeItem

class Game {
    +Company company;
    +List<Client> clients;
    +List<Employee> employees
    -{static}Game gameInstance
    +{static}Game getInstance()
    -void startTimer()
    +void hireEmployee(Employee)
    +void fireEmployee(Employee)
    +Contract getContract(Int)
    ..
    to initialize the game data
    ..
    -Game()
}

class Client {
  -Int id
  -String name
  -List<Contract> contracts
  -Int satifactory
  +Client(Int, String)
  +int getId()
  +String getName()
  +void addNewContract(Contract)
  +List<Contract> getContracts()
  +void satisfy(Int)
  +void dissatisfy(Int)
}

class Company {
    -String name;
    -Office office;
    -int balance;
    -List<Employee> employees;
    -List<Contract> contracts;
    -List<Client> clients;
    +Company(String, Office)
    +String getName()
    +Office getOffice()
    +List<Contract> getContracts()
    +contract getContract(String)
    +List<Client> getClients()
    +List<Employee> getEmployees()
    +void setOffice(Office)
    +void hireEmployee(Employee)
    +void acceptContract(Contract)
    +void addClient(Client)
    +void removeContract(Contract)
    +void removeClient(Client)
}

class Employee {
    -String name;
    -Int salary;
    -Int programmingSkill;
    -Int servicesSkill;
    -Int analysisSkill;
    -List<Profession> professions;
    +Employee(String, Int, Int, Int, Int)
    +String getName()
    +int getSalary()
    +int getProgrammingSkill()
    +int getServicesSkill()
    +int getAnalysisSkill()
    +void addProgrammingSkill(Int)
    +void addServicesSkill(Int)
    +void addAnalysisSkill(Int)
    +void addProfession(Profession)
    +List<Profession> getProfessions()
    +List<Profession> getProfessions(Category)
    +boolean isQualifiedForField(Category)
}

class Contract {
    -Int id;
    -String description;
    -Int time;
    -Int status;
    -Int profit;
    -Boolean finished;
    +Category category;
    +List<Employee> assignees;
    +Contract(Int, String, Int, Category)
    +int getId()
    +int getStatus()
    +int getProfit()
    +void assign(Employee)
    +void removeAssignee(Employee)
    +boolean isFinished()
    -void setFinished(Boolean)
    -void setStatus(status)
    +void advanceStatus()
    -Int getAdvance()
}

class Office {
    -Int rent
    -Int workSpots
    -Int level
    +Office(Int, Int, Int)
    +boolean canHire(Int)
    +int getRent()
}

class Profession {
    -String name;
    -Int programmingBoost;
    -Int analysisBoost;
    -Int servicesBoost;
    -Category category;
    +boolean hasBeenApplied;
    +Profession(String, Int, Int, Int, Category)
    +String getName()
    +int getProgrammingBoost()
    +int getAnalysisBoost()
    +int getServicesBoost()
}

class ContractListAdapter.ContractListAdapter {
    -List<Contract> contracts
    -Context context
    -{static}ViewHolder
    +ContractListAdapter(List<Contract>, Context)
    +ContractListAdapter.ViewHolder onCreateViewHolder(ViewGroup, Int)
    +void onBindViewHolder(ViewHolder, Int)
    +Int getItemCount()
}

class EmployeeListAdapter.EmployeeListAdapter {
    -List<Employee> employees
    -Context context
    -{static}ViewHolder
    +EmployeeListAdapter(List<Employee>, Context)
    +EmployeeListAdapter.ViewHolder onCreateViewHolder(ViewGroup, Int)
    +void onBindViewHolder(ViewHolder, Int)
    +Int getItemCount()
}

class ContractListAdapter.ViewHolder {
    -ContractItem contractItem
    -TextView contractTitle
    -ProgressBar contractProgress
    +ViewHolder(ContractItem) 
    +void setModel(Contract)
    +void onClick(View)
}

class EmployeeListAdapter.ViewHolder {
    -EmployeeItem employeeItem;
    -boolean internal;
    -TextView name;
    -TextView softwareSkill;
    -TextView serviceSkill;
    -TextView businessSkill;
    +ViewHolder(EmployeeItem) 
    +void setModel(Employee)
    +void onClick(View)
}

enum Category {
    +SE
    +BI
    +SM
}

@enduml


```


### Class reference

#### Game
This is, as said before, the class which holds all the data for the game to work with.
There are a lot of classes in the UML so that you can get a better understanding of the application
